#include <SFML/Graphics.hpp>

using namespace sf;

const int M = 20;
const int N = 10;
const int L = 4; // 方塊的顆粒個數

int field[M][N] = {
    0
};

struct Point {
    int x, y;
}
a[L], b[L];

// 不同長相的方塊
int figures[7][L] = {
    1,3,5,7, // I
    2,4,5,7, // Z
    3,5,4,6, // S
    3,5,4,7, // T
    2,3,5,7, // L
    3,5,7,6, // J
    2,3,4,5, // O
};

bool check() {
    for (int i = 0; i < L; i++)
        if (a[i].x < 0 || a[i].x >= N || a[i].y >= M) // 超過邊界
            return 0;
        else if (field[a[i].y][a[i].x]) // 發生碰撞，此位置已經有東西
            return 0;

    return 1;
};

int main() {
    srand(time(0));

    RenderWindow window(VideoMode(320, 480), "Tiles Game!");

    Texture tiles, bg, frm;
    tiles.loadFromFile("images/tiles.png");
    bg.loadFromFile("images/background.png");
    frm.loadFromFile("images/frame.png");

    Sprite s(tiles), background(bg), frame(frm);
    // (0, 0) : left, top 座標位置
    // (18, 18) : width, height 寬高
    s.setTextureRect(IntRect(0, 0, 18, 18));

    int dx = 0; // x 的位移
    bool rotate = 0; // 是否旋轉
    int colorNum = 1; // 哪個顏色
    float timer = 0, delay = 0.3;

    Clock clock;

    while (window.isOpen()) {
        // 上次 restart 到現在的時間間隔
        float time = clock.getElapsedTime().asSeconds();
        clock.restart();
        timer += time;

        Event e;
        while (window.pollEvent(e)) {
            if (e.type == Event::Closed) {
                window.close();
            }

            if (e.type == Event::KeyPressed) {
                if (e.key.code == Keyboard::Up)  // 上：旋轉
                    rotate = true;
                else if (e.key.code == Keyboard::Left)  // 左：x - 1
                    dx = -1;
                else if (e.key.code == Keyboard::Right)  // 右：x + 1
                    dx = 1;
            }
        }
        if (Keyboard::isKeyPressed(Keyboard::Down))
            delay = 0.05; // 減少更新的延遲

        //// <- 左右移動 -> ///
        for (int i = 0; i < L; i++) {
            b[i] = a[i];
            a[i].x += dx;
        }
        if (!check()) // 若超出邊界或碰撞則無法移動
            for (int i = 0; i < L; i++)
                a[i] = b[i];

        ////// <- 旋轉方塊 -> //////
        if (rotate) {
            Point p = a[1]; //center of rotation
            for (int i = 0; i < L; i++) {
                int x = a[i].y - p.y;
                int y = a[i].x - p.x;
                a[i].x = p.x - x;
                a[i].y = p.y + y;
            }
            if (!check()) // 若超出邊界或碰撞則無法移動
                for (int i = 0; i < L; i++)
                    a[i] = b[i];
        }

        int n = 3;  // T 形方塊 3,5,4,7
        if (a[0].x == 0) {
            for (int i = 0; i < L; i++) {
                // x: 1,1,0,1
                // y: 1,2,2,3
                a[i].x = figures[n][i] % 2;
                a[i].y = figures[n][i] / 2;
            }
        }

        /////// <- Tick 更新畫面 -> //////
        if (timer > delay) {
            for (int i = 0; i < L; i++) {
                b[i] = a[i];
                a[i].y += 1; // 向下移動
            }

            if (!check()) { // 超出畫面或衝突
                for (int i = 0; i < L; i++) // 將方塊的位置紀錄到 field 中
                    field[b[i].y][b[i].x] = colorNum;

                colorNum = 1 + rand() % 7; // 隨機選擇一個顏色
                int n = rand() % 7;  // 隨機選擇一種方塊形狀
                for (int i = 0; i < L; i++) {
                    // EX: figures[0] : 1,3,5,7 -> I
                    // x: 1,1,1,1
                    // y: 0,1,2,3
                    a[i].x = figures[n][i] % 2;
                    a[i].y = figures[n][i] / 2;
                }
            }

            timer = 0;
        }

        /////// <-- 移除完成的橫條 --> ///////////
        int k = M - 1;
        for (int i = M - 1; i > 0; i--) {
            int count = 0;
            for (int j = 0; j < N; j++) {
                if (field[i][j])
                    count++;
                field[k][j] = field[i][j];
            }
            if (count < N)
                k--;
        }

        // 重設變數
        dx = 0;
        rotate = 0;
        delay = 0.3;

        ///////// <-- draw 更新畫面 --> //////////
        window.clear(Color::White);
        window.draw(background);

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (field[i][j] == 0)
                    continue;
                s.setTextureRect(IntRect(field[i][j] * 18, 0, 18, 18));
                s.setPosition(j * 18, i * 18);
                s.move(28, 31); //offset
                window.draw(s);
            }
        }

        for (int i = 0; i < L; i++) {
            s.setTextureRect(IntRect(colorNum * 18, 0, 18, 18));
            s.setPosition(a[i].x * 18, a[i].y * 18);
            s.move(28, 31); //offset
            window.draw(s);
        }

        window.draw(frame);
        window.display();
    }

    return 0;
}
